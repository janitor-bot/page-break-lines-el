Source: page-break-lines-el
Section: editors
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Martin <debacle@debian.org>
Build-Depends: debhelper-compat (= 13),
	dh-elpa,
	elpa-package-lint
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/purcell/page-break-lines
Vcs-Git: https://salsa.debian.org/emacsen-team/page-break-lines-el.git
Vcs-Browser: https://salsa.debian.org/emacsen-team/page-break-lines-el

Package: elpa-page-break-lines
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs
Enhances: emacs
Description: Emacs mode to display ugly ^L page breaks as tidy horizontal lines
 This library provides an Emacs mode which displays form feed
 characters as horizontal rules.
 .
 The U+000C FORM FEED character is a normal white-space character, and
 in a text file is often used to mark virtual “page” separation.
 .
 Though it is rendered invisibly as white space, Emacs will (like many
 text editors) represent it with a glyph such as “^L”. This Emacs mode
 allows the same character to instead display as a custom horizontal
 line.
